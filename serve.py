# How Hot 2 - Flask Server
# (c) 2021  Owen McGinley
# Licensed under MIT License

from flask import Flask, render_template
import boto3
from configparser import ConfigParser

config = ConfigParser()
config.read("config.ini")

aws_access_key = config.read("AWS", "access_key")
aws_secret_key = config.read("AWS", "secret_key")
aws_region = config.read("AWS", "region")
aws_dbname = config.read("AWS", "dbname")

session = boto3.Session(aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key, region_name=aws_region)
db = session.resource('dynamodb')
table = db.Table(aws_dbname)

app = Flask(__name__)

@app.route("/", methods=['GET'])
def main_route():
    # Although not very OOP, at the end of the day, we just need to make separate dictionaries for the 5 data types,
    # then let the JS for each category handle it.
    response = table.scan()
    data_temp = {}
    data_hum = {}
    data_pres = {}
    data_aqi = {}
    data_lux = {}

    for i in response['Items']:
        data_temp[int(i['timestamp'])] = {"temp": float(i['insidetemp']), "tempd": float(i['insidetempdelta'])}
        try:
            data_hum[int(i['timestamp'])] = {"hum": float(i['insidehum']), "humd": float(i['insidehumd']), "dewpoint": float(i['indp'])}
        except KeyError:
            data_hum[int(i['timestamp'])] = {"hum": float(i['insidehum']), "humd": float(i['insidehumd'])}

        data_pres[int(i['timestamp'])] = {"pres": float(i['inpress'])}
        data_aqi[int(i['timestamp'])] = {"aqi": float(i['inaqi']), "pm10": float(i['inpm10']), "pm25": float(i['inpm25']), "pm100": float(i['inpm100'])}

    return render_template("base.html", datatemp=data_temp, datahum=data_hum, dataaqi=data_aqi, datapres=data_pres)


if __name__ == "__main__":
    app.run(debug=True)