# How Hot Is It In My Dorm Room 2

Web portal utilizing PyWeather 3 to answer the question - How Hot Is It In My Dorm Room?

# How Hot Is It In My Dorm Room 2 demo photos
They're on my site!

https://owenthe.dev/how-hot-is-it-in-my-dorm-room-2

# Why make How Hot Is It In My Dorm Room 2?
How Hot 2 is the second iteration of How Hot Is It In My Dorm Room. There were some necessary upgrades because PyWeather 3 now had additional data points from extra sensors.

However, I wanted to do some code cleanup, make things a bit more modular and object-oriented, and add some freatures here and there (like heat index support and dew point monitoring).

# Pre-requisities
How Hot 2 requires an active installation of PyWeather 3 (NOT PyWeather 2, that gets paired to How Hot 1) that is uploading to DynamoDB.

# Software Setup
On the software side, How Hot 2 requires
* Python 3.5 or higher
* Boto3 and Flask libraries
* A web server to serve How Hot 2 from (e.g. Apache with mod_wsgi)
* As mentioned earlier, an active PyWeather 3 install going to a DynamoDB database.

# Note
The How Hot 2 portal doesn't have light sensor data - I took it down due to potential privacy concerns. You'll need to make some code changes to re-enable light sensor support.

# Configuration Setup
config.ini contains all the configuration settings needed to get started with AWS.

If you configured PyWeather 3 with AWS upload enabled, you can transfer all the configuration values right over to How Hot 2.

## AWS section
`access_key` and `secret_key` are your access & secret keys that can read from the DynamoDB set up for data upload. These keys only need read access, if you prefer to use separate keys.

`region` is the region in which the DynamoDB database is being hosted (such as `us-east-2`).

`dbname` is the name of the DynamoDB database being used for data upload.

# Web Server Setup
You'll likely end up using a WSGI module to serve How Hot 2. I've included a simple serve.wsgi file that I used on my Apache2 web server.

No special configurations or tweaks are needed on the web server side to get everything running. The only thing I recommend is running in daemon mode for improved performance.

## Note about relative paths
If you get internal server errors when running How Hot for the first time, you may need to edit `serve.py` so that the configuration file doesn't use a relative path, and instead use an absolute path.

Once the web server is up (and some data exists in the DynamoDB database being used for upload), you should be good to go! You can now monitor indoor environmental data generated from a PyWeather 3 instance.

# Further customization
You'll probably want to make customizations to the title of the page, about sections, etc. The code here is what was running on my web server when this was up.

# License
How Hot Is It In My Dorm Room 2 is licensed under the MIT License.

How Hot Is It In My Dorm Room 2 includes a status button CSS library that I can't locate the original source for. As far as I remember, that was licensed under the MIT License (or something similar).
