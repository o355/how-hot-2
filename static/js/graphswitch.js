/*
  How Hot 2 - Graph Switcher
  (c) 2021  Owen McGinley
  Licensed under MIT License
*/

function graphSwitch(graph) {
    $("#temp-graph").hide()
    $("#hum-graph").hide()
    $("#aqi-graph").hide()
    $("#pres-graph").hide()
    $("#lux-graph").hide()

    if (graph == "temp") {
        $("#temp-graph").show()
        temphtml.rendergraph()
    } else if (graph == "humidity") {
        $("#hum-graph").show()
        humhtml.rendergraph()
    } else if (graph == "aqi") {
        $("#aqi-graph").show()
        aqihtml.rendergraph()
    } else if (graph == "pressure") {
        $("#pres-graph").show()
        preshtml.rendergraph()
    }
}