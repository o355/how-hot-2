/*
  How Hot 2 - Pressure Class
  (c) 2021  Owen McGinley
  Licensed under MIT License
*/

class Pressure {
    constructor (data, h3, deltah5, graphpres, graphdelta24) {
        this.data = data
        this.h3 = h3
        this.deltah5 = deltah5
        this.graphpres = graphpres
        this.graphdelta24 = graphdelta24
        this.newestts = 0
        this.key
        
        for (this.key in this.data) {
            if (this.key > this.newestts) {
                this.newestts = this.key
            }
        }
    }

    populate() {

        $(this.h3).append(this.data[this.newestts]['pres'].toFixed(2) + " mb")
        
        try {
            $(this.deltah5).html((this.data[this.newestts]['pres'] - this.data[this.newestts - 86400]['pres']).toFixed(2))
        } catch {
            $(this.deltah5).html("N/A")
        }
    }

    rendergraph() {
        var key;
        var options = {
            hAxis: {
                title: 'Time',
                format: 'M/d, h a',
            },
            vAxis: {
                format: '#.## mb',
            },
            legend: {
                position: 'none',
            }
        }
        var dateformatter = new google.visualization.DateFormat({pattern: 'MMMM d, h:mm a'});
        var numformatter = new google.visualization.NumberFormat({suffix: ' mb'})
        // Pressure graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'Pressure')

        for (key in this.data) {
            data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pres'].toFixed(2))])
        }

        dateformatter.format(data, 0)
        numformatter.format(data, 1)
        var chart = new google.charts.Line(document.getElementById(this.graphpres))
        chart.draw(data, google.charts.Line.convertOptions(options))

        // Pressure delta graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'Pressure')

        for (key in this.data) {
            try {
                data.addRow([new Date(key * 1000), 
                    parseFloat((this.data[key]['pres'] - this.data[key - 86400]['pres']).toFixed(2))])
            } catch {
                continue
            }
        }

        dateformatter.format(data, 0)
        numformatter.format(data, 1)
        var chart = new google.charts.Line(document.getElementById(this.graphdelta24))
        chart.draw(data, google.charts.Line.convertOptions(options))
    }
}