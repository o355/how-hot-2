/*
  How Hot 2 - Temperature Class
  (c) 2021  Owen McGinley
  Licensed under MIT License
*/

class Temperature {
    constructor (data, h3, outsideh5, deltah5, lastupdated, graphtemp, graphdelta24, graphoutside, heatindex) {
        this.data = data
        console.log(this.data)
        this.h3 = h3
        this.outsideh5 = outsideh5
        this.deltah5 = deltah5
        this.graphtemp = graphtemp
        this.graphdelta24 = graphdelta24
        this.graphoutside = graphoutside
        this.newestts = 0
        this.key;
        this.lastupdated = lastupdated
        this.heatindex = heatindex

        for (this.key in this.data) {
            if (this.key > this.newestts) {
                this.newestts = this.key
            }
        }

        this.snowflake_cutoff = 60
        this.coldface_cutoff = 63
        this.cold_cutoff = 66
        this.green_cutoff = 74
        this.yellow_cutoff = 80
        this.sweat_cutoff = 83
        this.fire_cutoff = 86
        this.steak_cutoff = 95
        this.brick_cutoff = 105
        this.skull_cutoff = 115
    }

    getlatest() {
        return this.data[this.newestts]['temp']
    }

    populate() {

        if (this.data[this.newestts]['temp'] < this.snowflake_cutoff) {
            $(this.h3).append("❄️&nbsp;")
            $(this.h3).removeClass("h3style")
            $(this.h3).addClass("h3style-fire")
        } else if (this.data[this.newestts]['temp'] < this.coldface_cutoff) {
            $(this.h3).append("🥶&nbsp;")
            $(this.h3).removeClass("h3style")
            $(this.h3).addClass("h3style-fire")
        } else if (this.data[this.newestts]['temp'] < this.cold_cutoff) {
            $(this.h3).append("<status-indicator active pulse></status-indicator>&nbsp; ")
        } else if (this.data[this.newestts]['temp'] < this.green_cutoff) {
            $(this.h3).append("<status-indicator positive pulse></status-indicator>&nbsp; ")
        } else if (this.data[this.newestts]['temp'] < this.yellow_cutoff) {
            $(this.h3).append("<status-indicator intermediary pulse></status-indicator>&nbsp; ")
        } else if (this.data[this.newestts]['temp'] < this.sweat_cutoff) {
            $(this.h3).append("<status-indicator negative pulse></status-indicator>&nbsp; ")
        } else if (this.data[this.newestts]['temp'] < this.fire_cutoff) {
            $(this.h3).append("🥵&nbsp;")
            $(this.h3).removeClass("h3style")
            $(this.h3).addClass("h3style-fire")
        } else if (this.data[this.newestts]['temp'] < this.steak_cutoff) {
            $(this.h3).append("🔥&nbsp;")
            $(this.h3).removeClass("h3style")
            $(this.h3).addClass("h3style-fire")
        } else if (this.data[this.newestts]['temp'] < this.brick_cutoff) {
            $(this.h3).append("🍖&nbsp;")
            $(this.h3).removeClass("h3style")
            $(this.h3).addClass("h3style-fire")
        } else if (this.data[this.newestts]['temp'] < this.skull_cutoff) {
            $(this.h3).append("🧱&nbsp;")
            $(this.h3).removeClass("h3style")
            $(this.h3).addClass("h3style-fire")
        } else {
            $(this.h3).append("☠️&nbsp;")
            $(this.h3).removeClass("h3style")
            $(this.h3).addClass("h3style-fire")
        }
        console.log(this.newestts)
        $(this.h3).append(this.data[this.newestts]['temp'].toFixed(2) + "°F")
        $(this.outsideh5).html(this.data[this.newestts]['tempd'].toFixed(2))

        try {
            $(this.deltah5).html((this.data[this.newestts]['temp'] - this.data[this.newestts - 86400]['temp']).toFixed(2))
        } catch {
            $(this.deltah5).html("N/A")
        }

        var dateobj = new Date(this.newestts * 1000)
        var datestring = dateobj.toLocaleString(navigator.language, {year: 'numeric', month: 'long', day: 'numeric'})
        var timestring = dateobj.toLocaleString(navigator.language, {hour: 'numeric', minute: 'numeric'})
        $(this.lastupdated).html(datestring + " at " + timestring)
    }

    rendergraph() {
        var key;
        var options = {
            hAxis: {
                title: 'Time',
                format: 'M/d, h a',
            },
            vAxis: {
                format: '#.##°F',
            },
            legend: {
                position: 'none',
            }
        }
        var dateformatter = new google.visualization.DateFormat({pattern: 'MMMM d, h:mm a'});
        var numformatter = new google.visualization.NumberFormat({suffix: '°F'})
        // Temp graph
        var data = new google.visualization.DataTable()
        if (this.heatindex.showheatindex()) {
            data.addColumn('date', 'Time')
            data.addColumn('number', 'Temperature')
            data.addColumn('number', 'Heat Index')

            for (key in this.data) {
                var heatindex_temp = this.heatindex.getheatindex(key)
                if (heatindex_temp != null) {
                    heatindex_temp = parseFloat(heatindex_temp.toFixed(2))
                }
                data.addRow([new Date(key * 1000), parseFloat(this.data[key]['temp'].toFixed(2)), heatindex_temp])
            }
            dateformatter.format(data, 0)
            numformatter.format(data, 1)
            numformatter.format(data, 2)
        } else {
            data.addColumn('date', 'Time')
            data.addColumn('number', 'Temperature')

            for (key in this.data) {
                data.addRow([new Date(key * 1000), parseFloat(this.data[key]['temp'].toFixed(2))])
            }
            dateformatter.format(data, 0)
            numformatter.format(data, 1)
        }

        var chart = new google.charts.Line(document.getElementById(this.graphtemp))
        chart.draw(data, google.charts.Line.convertOptions(options))

        // 24h delta graph
        data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'ΔT')

        for (key in this.data) {
            try {
                data.addRow([new Date(key * 1000), parseFloat((this.data[key]['temp'] - this.data[key - 86400]['temp']).toFixed(2))])
            } catch {
                continue
            }
        }

        dateformatter.format(data, 0)
        numformatter.format(data, 1)
        var chart = new google.charts.Line(document.getElementById(this.graphdelta24))
        chart.draw(data, google.charts.Line.convertOptions(options))

        // Outside delta graph
        data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'ΔT')

        for (key in this.data) {
            data.addRow([new Date(key * 1000), parseFloat(this.data[key]['tempd'].toFixed(2))])
        }

        dateformatter.format(data, 0)
        numformatter.format(data, 1)
        var chart = new google.charts.Line(document.getElementById(this.graphoutside))
        chart.draw(data, google.charts.Line.convertOptions(options))
    }
}