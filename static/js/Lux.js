/*
  How Hot 2 - Lux Class
  (c) 2021  Owen McGinley
  Licensed under MIT License
*/

class Lux {
    constructor (data, h3, deltah5, luxgraph, lux24graph) {
        this.data = data
        this.h3 = h3
        this.deltah5 = deltah5
        this.luxgraph = luxgraph
        this.lux24graph = lux24graph
        this.newestts = 0
        this.key

        for (this.key in this.data) {
            if (this.key > this.newestts) {
                this.newestts = this.key
            }
        }
    }

    populate() {
        $(this.h3).append(Math.round(this.data[this.newestts]['lux']) + " lux")
        
        try {
            $(this.deltah5).html(Math.round(this.data[this.newestts]['lux'] - this.data[this.newestts - 86400]['lux']))
        } catch {
            $(this.deltah5).html("N/A")
        }
    }

    rendergraph() {
        var key;
        var options = {
            hAxis: {
                title: 'Time',
                format: 'M/d, h a',
            },
            vAxis: {
                format: '#.## lux',
            },
            legend: {
                position: 'none',
            }
        }
        var dateformatter = new google.visualization.DateFormat({pattern: 'MMMM d, h:mm a'});
        var numformatter = new google.visualization.NumberFormat({suffix: ' lux'})
        // Lux graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'Light Level')

        for (key in this.data) {
            data.addRow([new Date(key * 1000), parseFloat(this.data[key]['lux'].toFixed(2))])
        }

        dateformatter.format(data, 0)
        numformatter.format(data, 1)
        var chart = new google.charts.Line(document.getElementById(this.luxgraph))
        chart.draw(data, google.charts.Line.convertOptions(options))

        // 24h delta graph
        data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'Δlux')
        key = null

        for (key in this.data) {
            try {
                var lux_now = parseFloat(this.data[key]['lux'])
                var lux_past = parseFloat(this.data[key - 86400]['lux'])
                var lux_diff = lux_now - lux_past
                var lux_diff = lux_diff.toFixed(2)
                data.addRow([new Date(key * 1000), parseFloat(lux_diff)])
            } catch (err) {
                continue
            }
        }

        dateformatter.format(data, 0)
        numformatter.format(data, 1)
        var chart = new google.charts.Line(document.getElementById(this.lux24graph))
        chart.draw(data, google.charts.Line.convertOptions(options))
    }
}