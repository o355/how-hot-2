/*
  How Hot 2 - Humidity Class
  (c) 2021  Owen McGinley
  Licensed under MIT License
*/

class Humidity {
    constructor (data, h3, outsideh5, deltah5, dph5, graphhum, graphdelta24, graphoutside, graphdp) {
        this.data = data
        this.h3 = h3
        this.outsideh5 = outsideh5
        this.deltah5 = deltah5
        this.graphhum = graphhum
        this.graphdelta24 = graphdelta24
        this.graphoutside = graphoutside
        this.dph5 = dph5
        this.graphdp = graphdp
        this.newestts = 0
        this.key

        for (this.key in this.data) {
            if (this.key > this.newestts) {
                this.newestts = this.key
            }
        }

        this.desertcutoff = 10
        this.redlowcutoff = 15
        this.yellowlowcutoff = 30
        this.greencutoff = 60
        this.yellowhighcutoff = 70
    }

    getlatest() {
        return this.data[this.newestts]['hum']
    }
 
    populate() {
        if (this.data[this.newestts]['hum'] < this.desertcutoff) {
            $(this.h3).append("🏜️&nbsp;")
            $(this.h3).removeClass("h3style")
            $(this.h3).addClass("h3style-fire")
        } else if (this.data[this.newestts]['hum'] < this.redlowcutoff) {
            $(this.h3).append("<status-indicator negative pulse></status-indicator>&nbsp; ")
        } else if (this.data[this.newestts]['hum'] < this.yellowlowcutoff) {
            $(this.h3).append("<status-indicator intermediary pulse></status-indicator>&nbsp; ")
        } else if (this.data[this.newestts]['hum'] < this.greencutoff) {
            $(this.h3).append("<status-indicator positive pulse></status-indicator>&nbsp; ")
        } else if (this.data[this.newestts]['hum'] < this.yellowhighcutoff) {
            $(this.h3).append("<status-indicator intermediary pulse></status-indicator>&nbsp; ")
        } else {
            $(this.h3).append("<status-indicator negative pulse></status-indicator>&nbsp; ")
        }

        $(this.h3).append(this.data[this.newestts]['hum'].toFixed(2) + "%")
        $(this.outsideh5).html(this.data[this.newestts]['humd'].toFixed(2))
        $(this.dph5).html(this.data[this.newestts]['dewpoint'].toFixed(2))
        try {
            $(this.deltah5).html((this.data[this.newestts]['hum'] - this.data[this.newestts - 86400]['hum']).toFixed(2))
        } catch {
            $(this.deltah5).html("N/A")
        }

        try {
            $(this.dpdeltah5).html((this.data[this.newestts]['dewpoint'] - this.data[this.newestts - 86400]['dewpoint']).toFixed(2))
        } catch {
            $(this.dpdeltah5).html("N/A")
        }
    }

    rendergraph() {
        var key;
        var options = {
            hAxis: {
                title: 'Time',
                format: 'M/d, h a',
            },
            vAxis: {
                format: '#.##' + "'%'",
            },
            legend: {
                position: 'none',
            }
        }

        var options_dp = {
            hAxis: {
                title: 'Time',
                format: 'M/d, h a',
            },
            vAxis: {
                format: '#.##°F',
            },
            legend: {
                position: 'none',
            }
        }
        var dateformatter = new google.visualization.DateFormat({pattern: 'MMMM d, h:mm a'});
        var numformatter = new google.visualization.NumberFormat({suffix: '%'})
        var tempformatter = new google.visualization.NumberFormat({suffix: '°F'})
        // Humidity graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'Humidity')

        for (key in this.data) {
            data.addRow([new Date(key * 1000), parseFloat(this.data[key]['hum'].toFixed(2))])
        }

        dateformatter.format(data, 0)
        numformatter.format(data, 1)
        var chart = new google.charts.Line(document.getElementById(this.graphhum))
        chart.draw(data, google.charts.Line.convertOptions(options))

        // Humidity delta graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'ΔRH')

        for (key in this.data) {
            try {
                data.addRow([new Date(key * 1000), parseFloat((this.data[key]['hum'] - this.data[key - 86400]['hum']).toFixed(2))])
            } catch {
                continue
            }
        }

        dateformatter.format(data, 0)
        numformatter.format(data, 1)
        var chart = new google.charts.Line(document.getElementById(this.graphdelta24))
        chart.draw(data, google.charts.Line.convertOptions(options))

        // 24h Humidity Delta graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'ΔRH')

        for (key in this.data) {
            data.addRow([new Date(key * 1000), parseFloat(this.data[key]['humd'].toFixed(2))])
        }

        dateformatter.format(data, 0)
        numformatter.format(data, 1)
        var chart = new google.charts.Line(document.getElementById(this.graphoutside))
        chart.draw(data, google.charts.Line.convertOptions(options))

        // Dew Point graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'Dew Point')

        for (key in this.data) {
            try {
                data.addRow([new Date(key * 1000), parseFloat(this.data[key]['dewpoint'].toFixed(2))])
            } catch {
                continue
            }
        }
        
        dateformatter.format(data, 0)
        tempformatter.format(data, 1)
        var chart = new google.charts.Line(document.getElementById(this.graphdp))
        chart.draw(data, google.charts.Line.convertOptions(options_dp))
    }
}