/*
  How Hot 2 - Air Quality Class
  (c) 2021  Owen McGinley
  Licensed under MIT License
*/

class AirQuality {
    constructor (data, h3, pm1h5, pm25h5, pm100h5, deltah5, graphaqi, graphdeltaaqi, graphpm1, graphpm1delta, graphpm25, graphpm25delta, graphpm100, graphpm100delta) {
        this.data = data
        this.h3 = h3
        this.pm1h5 = pm1h5
        this.pm25h5 = pm25h5
        this.pm100h5 = pm100h5
        this.deltah5 = deltah5
        this.graphaqi = graphaqi
        this.graphdeltaaqi = graphdeltaaqi
        this.graphpm1 = graphpm1
        this.graphpm1delta = graphpm1delta
        this.graphpm25 = graphpm25
        this.graphpm25delta = graphpm25delta
        this.graphpm100 = graphpm100
        this.graphpm100delta = graphpm100delta
        this.newestts = 0
        this.key
        
        for (this.key in this.data) {
            if (this.key > this.newestts) {
                this.newestts = this.key
            }
        }

        this.greencutoff = 51
        this.yellowcutoff = 101
    }

    l6r(l6) {
        return parseFloat(l6.reduce((a, b) => a + b) / 6)
    }

    populate() {
        if (Math.round(this.data[this.newestts]['aqi']) < this.greencutoff) {
            $(this.h3).append("<status-indicator positive pulse></status-indicator>&nbsp; ")
        } else if (Math.round(this.data[this.newestts]['aqi']) < this.yellowcutoff) {
            $(this.h3).append("<status-indicator intermediary pulse></status-indicator>&nbsp; ")
        } else {
            $(this.h3).append("<status-indicator negative pulse></status-indicator>&nbsp; ")
        }

        $(this.h3).append(Math.round(this.data[this.newestts]['aqi']) + " AQI")
        $(this.pm1h5).html(this.data[this.newestts]['pm10'])
        $(this.pm25h5).html(this.data[this.newestts]['pm25'])
        $(this.pm100h5).html(this.data[this.newestts]['pm100'])

        try {
            $(this.deltah5).html(Math.round(this.data[this.newestts]['aqi'] - this.data[this.newestts - 86400]['aqi']))
        } catch {
            $(this.deltah5).html("N/A")
        }
    }

    rendergraph() {
        var key;
        var options_pm = {
            hAxis: {
                title: 'Time',
                format: 'M/d, h a',
            },
            vAxis: {
                format: '#.## µg/m3',
            },
            legend: {
                position: 'none',
            }
        }

        var options_aqi = {
            hAxis: {
                title: 'Time',
                format: 'M/d, h a',
            },
            vAxis: {
                format: '#.## AQI',
            },
            legend: {
                position: 'none',
            }
        }

        var dateformatter = new google.visualization.DateFormat({pattern: 'MMMM d, h:mm a'});
        var numformatter_pm = new google.visualization.NumberFormat({suffix: ' µg/m3'})
        var numformatter_aqi = new google.visualization.NumberFormat({suffix: ' AQI', fractionDigits: 2})
        // AQI graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'AQI')
        data.addColumn('number', 'Average AQI (30 mins)')

        var last6 = []


        for (key in this.data) {
            last6.push(parseFloat(Math.round(this.data[key]['aqi'])))
            if (last6.length == 6) {
                data.addRow([new Date(key * 1000), parseFloat(Math.round(this.data[key]['aqi'])), parseFloat(last6.reduce((a, b) => a + b) / 6)])
                last6.shift()
            } else {
                data.addRow([new Date(key * 1000), parseFloat(Math.round(this.data[key]['aqi'])), null])
            }
        }

        dateformatter.format(data, 0)
        numformatter_aqi.format(data, 1)
        numformatter_aqi.format(data, 2)
        var chart = new google.charts.Line(document.getElementById(this.graphaqi))
        chart.draw(data, google.charts.Line.convertOptions(options_aqi))

        // AQI 24h delta graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'ΔAQI')
        data.addColumn('number', 'Average ΔAQI (30 mins)')

        var last6 = []

        for (key in this.data) {
            try {
                last6.push(parseFloat(Math.round((this.data[key]['aqi'] - this.data[key - 86400]['aqi']).toFixed(2))))
                if (last6.length == 6) {
                    data.addRow([new Date(key * 1000), parseFloat(Math.round((this.data[key]['aqi'] - this.data[key - 86400]['aqi']).toFixed(2))), parseFloat(last6.reduce((a, b) => a + b) / 6)])
                    last6.shift()
                } else {
                    data.addRow([new Date(key * 1000), parseFloat(Math.round((this.data[key]['aqi'] - this.data[key - 86400]['aqi']).toFixed(2))), null])
                }
            } catch {
                continue
            }
        }

        dateformatter.format(data, 0)
        numformatter_aqi.format(data, 1)
        numformatter_aqi.format(data, 2)
        var chart = new google.charts.Line(document.getElementById(this.graphdeltaaqi))
        chart.draw(data, google.charts.Line.convertOptions(options_aqi))

        // PM1.0 graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'PM1 Concentration')
        data.addColumn('number', 'Average PM1 (last 30 mins)')

        var last6 = []

        for (key in this.data) {
            last6.push(parseFloat(this.data[key]['pm10']))
            if (last6.length == 6) {
                data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm10']), this.l6r(last6)])
                last6.shift()
            } else {
                data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm10']), null])
            }
        }

        dateformatter.format(data, 0)
        numformatter_pm.format(data, 1)
        numformatter_pm.format(data, 2)
        var chart = new google.charts.Line(document.getElementById(this.graphpm1))
        chart.draw(data, google.charts.Line.convertOptions(options_pm))

        // PM 1.0 24h delta graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'ΔPM1 Concentration')
        data.addColumn('number', 'Average ΔPM1 Concentration (last 30 mins)')

        var last6 = []

        for (key in this.data) {
            try {
                last6.push(parseFloat(this.data[key]['pm10'] - this.data[key - 86400]['pm10']))
                if (last6.length == 6) {
                    data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm10'] - this.data[key - 86400]['pm10']), this.l6r(last6)])
                    last6.shift()
                } else {
                    data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm10'] - this.data[key - 86400]['pm10']), null])
                }
            } catch {
                continue
            }
        }

        dateformatter.format(data, 0)
        numformatter_pm.format(data, 1)
        numformatter_pm.format(data, 2)
        var chart = new google.charts.Line(document.getElementById(this.graphpm1delta))
        chart.draw(data, google.charts.Line.convertOptions(options_pm))

        // PM2.5 graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'PM2.5 Concentration')
        data.addColumn('number', 'Average PM2.5 (last 30 mins)')

        var last6 = []

        for (key in this.data) {
            last6.push(parseFloat(this.data[key]['pm25']))
            if (last6.length == 6) {
                data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm25']), this.l6r(last6)])
                last6.shift()
            } else {
                data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm25']), null])
            }
        }

        dateformatter.format(data, 0)
        numformatter_pm.format(data, 1)
        numformatter_pm.format(data, 2)
        var chart = new google.charts.Line(document.getElementById(this.graphpm25))
        chart.draw(data, google.charts.Line.convertOptions(options_pm))

        // PM 2.5 24h delta graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'ΔPM2.5 Concentration')
        data.addColumn('number', 'Average ΔPM2.5 Concentration (last 30 mins)')

        var last6 = []

        for (key in this.data) {
            try {
                last6.push(parseFloat(this.data[key]['pm25'] - this.data[key - 86400]['pm25']))
                if (last6.length == 6) {
                    data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm25'] - this.data[key - 86400]['pm25']), this.l6r(last6)])
                    last6.shift()
                } else {
                    data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm25'] - this.data[key - 86400]['pm25']), null])
                }
            } catch {
                continue
            }
        }

        dateformatter.format(data, 0)
        numformatter_pm.format(data, 1)
        numformatter_pm.format(data, 2)
        var chart = new google.charts.Line(document.getElementById(this.graphpm25delta))
        chart.draw(data, google.charts.Line.convertOptions(options_pm))

        // PM10 graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'PM10 Concentration')
        data.addColumn('number', 'Average PM10 (last 30 mins)')

        var last6 = []

        for (key in this.data) {
            last6.push(parseFloat(this.data[key]['pm100']))
            if (last6.length == 6) {
                data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm100']), this.l6r(last6)])
                last6.shift()
            } else {
                data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm100']), null])
            }
        }

        dateformatter.format(data, 0)
        numformatter_pm.format(data, 1)
        numformatter_pm.format(data, 2)
        var chart = new google.charts.Line(document.getElementById(this.graphpm100))
        chart.draw(data, google.charts.Line.convertOptions(options_pm))

        // PM 10 24h delta graph
        var data = new google.visualization.DataTable()
        data.addColumn('date', 'Time')
        data.addColumn('number', 'ΔPM10 Concentration')
        data.addColumn('number', 'Average ΔPM10 Concentration (last 30 mins)')

        var last6 = []

        for (key in this.data) {
            try {
                last6.push(parseFloat(this.data[key]['pm100'] - this.data[key - 86400]['pm100']))
                if (last6.length == 6) {
                    data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm100'] - this.data[key - 86400]['pm100']), this.l6r(last6)])
                    last6.shift()
                } else {
                    data.addRow([new Date(key * 1000), parseFloat(this.data[key]['pm100'] - this.data[key - 86400]['pm100']), null])
                }
            } catch {
                continue
            }
        }

        dateformatter.format(data, 0)
        numformatter_pm.format(data, 1)
        numformatter_pm.format(data, 2)
        var chart = new google.charts.Line(document.getElementById(this.graphpm100delta))
        chart.draw(data, google.charts.Line.convertOptions(options_pm))
    }
}